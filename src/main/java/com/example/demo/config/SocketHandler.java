package com.example.demo.config;



import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


@Component
public class SocketHandler extends TextWebSocketHandler {

    List<WebSocketSession> sessions = new CopyOnWriteArrayList<WebSocketSession>();
    ArrayList<String> listOfMessages = new ArrayList<>();

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message)
            throws InterruptedException, IOException {
        listOfMessages.add(message.getPayload());
        for(WebSocketSession webSocketSession : sessions) {
            System.out.println(" ------------------------------------------------------------------------------ ");
            System.out.println(message.getPayload() + "<= Tekst ktory wyslal jakis uzytkownik ");
            System.out.println(" ------------------------------------------------------------------------------ ");
            webSocketSession.sendMessage(new TextMessage(  message.getPayload() ));

        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        //the messages will be broadcasted to all users.
        sessions.add(session);
        session.sendMessage(new TextMessage("{\"userName\":\"Admin\",\"text\":\"witaj\"}"));
        if(!listOfMessages.isEmpty()){
            listOfMessages.forEach(s -> {
                try {
                    session.sendMessage(new TextMessage(s));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception{
        sessions.remove(session);
    }
}
